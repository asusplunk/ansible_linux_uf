================================================================
Ansible Playbook to install Splunk Universal Forwarders on Linux
================================================================

This Playbook installs the Splunk Universal Forwarder (version 7.0.3) and any apps placed in :code:`files/etc/apps`.

Requirements
============
- Red-Hat- or Debian-based system (tested on CentOS 6 and 7; Ubuntu 14.04 and 16.04)
- Wget
- Python bindings for SELinux (libselinux-python on RHEL, python-selinux on Ubuntu)
- Git
- Ansible (installing from the OS's package manager instead of pip works best, though this requires EPEL on RHEL)

Usage
=====
1. Clone this repository to the machine you wish to forward from.
2. :code:`./install_uf.yml`
3. Contact the Splunk Admins to finish configuring your forwarder.

Security Note
=============
This playbook sets the universal forwarder's admin password to a default value defined in :code:`vars.yml`.

You may wish to change this value to something unique before you run the playbook, if you're worried about others with this access to this playbook knowing it.